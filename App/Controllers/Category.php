<?php

use App\Core\Controller;
use App\Models\Category as ModelsCategory;

class Category extends Controller
{
    /** Receber registros do banco de dados @var array */
    public array $data;

    /** recebe os dados do formulário @var array */
    private array $dataForm;

    /** recebe o id por parametro @var integer */
    private int $id;

    public function index()
    {
        $categories = new ModelsCategory();
        $this->data['Categories'] = $categories->getAll();

        $this->loadView("categories", $this->data['Categories']);
    }

    public function add()
    {
        if (isset($_POST['save'])) {
            $this->dataForm = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            unset($this->dataForm['save']);

            $addCategory = new ModelsCategory();
            $addCategory->addNew($this->dataForm);

            if ($addCategory->getResult()) {
                header("Location: /category/index");
            } else {
                $this->loadView("addCategory");
            }
        }

        $this->loadView("addCategory");
    }

    public function edit($id = '')
    {
        $this->id = (int) $id;

        $viewCategory = new ModelsCategory();
        $this->data['CategoryId'] = $viewCategory->getCategoryByid($this->id);

        if ((!empty($this->data['CategoryId'])) and (isset($_POST['update']))) {

            $this->dataForm = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            unset($this->dataForm['update']);

            $editCategory = new ModelsCategory();
            $editCategory->update($this->dataForm);

            if ($editCategory->update($this->dataForm)) {
                header("Location: /category/index");
            } else {
                $this->loadView("addCategory", $this->data['CategoryId']);
            }
        }

        $this->loadView("addCategory", $this->data['CategoryId']);
    }

    public function delete($id = '')
    {
        $this->id = (int) $id;

        $deleteCategory = new ModelsCategory();
        $deleteCategory->deleteCategory($this->id);

        if ($deleteCategory->deleteCategory($this->id)) {
            header("Location: /category/index");
        } else {
            $this->loadView("addCategory", $this->data['CategoryId']);
        }
    }
}
