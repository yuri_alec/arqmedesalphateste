<?php

use App\Core\Controller;
use App\Models\Products;

class Home extends Controller
{
    /** Receber registros @var array */
    public array $data;

    public function index()
    {
        $products = new Products();
        $this->data['Products'] = $products->getAll();

        $total = new Products();
        $this->data['Total'] = $total->totalRocords();

        $this->loadView("dashboard", [$this->data['Products'], $this->data['Total']]);
    }
}
