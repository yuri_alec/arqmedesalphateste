<?php

use App\Core\Controller;
use App\Models\Category;
use App\Models\Products as ModelsProducts;
use Upload\File;
use Upload\Storage\FileSystem;
use Upload\Validation\Mimetype;
use Upload\Validation\Size;

class Products extends Controller
{
    /** Receber registros do banco de dados @var array */
    public array $data;

    /** recebe os dados do formulário @var array */
    private array $dataForm;

    /** Recebe a imagem @var */
    private $image;

    /** recebe o id por parametro @var integer */
    private int $id;

    public function index()
    {
        $products = new ModelsProducts();
        $this->data['Products'] = $products->getAll();

        $this->loadView("products", $this->data['Products']);
    }

    public function add()
    {
        //Carregar categorias
        $categories = new Category();
        $this->data['categories'] = $categories->getAll();


        $this->loadView("addProduct", $this->data['categories']);

        if ($_POST['addNewProduct']) {
            $this->dataForm = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            unset($this->dataForm['addNewProduct']);

            /** Upload da imagem */
            $storage = new FileSystem('uploads');

            $file = new File('image', $storage);

            // Optionally you can rename the file on upload
            $new_filename = uniqid();
            $file->setName($new_filename);

            // Validate file upload
            $file->addValidations(array(
                new Mimetype(array('image/png', 'image/jpg', 'image/jpeg')),
                new Size('5M')
            ));

            // Access data about the file that has been uploaded
            $data = array(
                'name'       => $file->getNameWithExtension(),
                'extension'  => $file->getExtension(),
                'mime'       => $file->getMimetype(),
                'size'       => $file->getSize(),
                'md5'        => $file->getMd5(),
                'dimensions' => $file->getDimensions()
            );

            // Try to upload file
            try {
                // Success!
                $file->upload();

                $this->image = $data['name'];

                $newProduct = new ModelsProducts;
                $newProduct->add($this->dataForm, $this->image);

                if ($newProduct->getResult()) {
                    header("Location: http://localhost:8009/home/");
                } else {
                    header("Location: http://localhost:8009/products/add");
                }
            } catch (\Exception $e) {
                // Fail!
                $errors = $file->getErrors();
                $_SESSION['msg'] = "<p><span style='color: #f00'>Couldn't save. Try again later.</span><p>";
                header("Location: /home/index");
            }
        }
    }

    public function edit($id = '')
    {
        $this->id = (int) $id;

        $viewProduct = new ModelsProducts();
        $this->data['ProductId'] = $viewProduct->getProductByid($this->id);

        $categories = new Category();
        $this->data['categories'] = $categories->getAll();

        if ((!empty($this->data['ProductId'])) and (isset($_POST['update']))) {

            $this->dataForm = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            unset($this->dataForm['update']);

            /** Upload da imagem */
            $storage = new FileSystem('uploads');

            $file = new File('image', $storage);

            // Optionally you can rename the file on upload
            $new_filename = uniqid();
            $file->setName($new_filename);

            // Validate file upload
            $file->addValidations(array(
                new Mimetype(array('image/png', 'image/jpg', 'image/jpeg')),
                new Size('5M')
            ));

            // Access data about the file that has been uploaded
            $data = array(
                'name'       => $file->getNameWithExtension(),
                'extension'  => $file->getExtension(),
                'mime'       => $file->getMimetype(),
                'size'       => $file->getSize(),
                'md5'        => $file->getMd5(),
                'dimensions' => $file->getDimensions()
            );

            // Try to upload file
            try {
                // Success!
                $file->upload();

                $this->image = $data['name'];

                $editProduct = new ModelsProducts();
                $editProduct->edit($this->dataForm, $this->image);

                if ($editProduct->getResult()) {
                    header("Location: http://localhost:8009/");
                } else {
                    header("Location: http://localhost:8009/products/edit");
                }
            } catch (\Exception $e) {
                // Fail!
                $errors = $file->getErrors();
                $_SESSION['msg'] = "<p><span style='color: #f00'>Couldn't save. Try again later.</span><p>";
                header("Location: /home/index");
            }
        }

        $this->loadView("addProduct", [$this->data['CategoryId'], $this->data['categories']]);
    }

    public function delete($id = '')
    {
        $this->id = (int) $id;

        $deleteProduct = new ModelsProducts();
        $deleteProduct->delete($this->id);

        if ($deleteProduct->getResult()) {
            header("Location: http://localhost:8009/");
        } else {
            $this->loadView("addCategory", $this->data['CategoryId']);
        }
    }
}
