<?php

namespace App\Core;

/**
 * O fluxo da aplicação inicia na classe app;
 * Sistema de rotas;
 */
class App
{
    protected $urlController = "Home";
    protected string $urlMethod = "index";
    protected array $urlParam = [];

    public function __construct()
    {
        //Recebe a url
        $url = $this->parseUrl();

        // Verifica se o arquivo existe
        if (file_exists('../App/Controllers/' . $url[1] . '.php')) {
            $this->urlController = $url[1];
            unset($url[1]);
        }

        //Importar a classe caso exista
        require_once '../App/Controllers/' . $this->urlController . '.php';

        //O atributo $urlController se torna um novo objeto para instanciar o metodo
        $this->urlController = new $this->urlController;

        //Verifica se existe o segundo parametro, caso exista, o atributo $urlMethod recebe o valor
        if (isset($url[2])) {
            if (method_exists($this->urlController, $url[2])) {
                $this->urlMethod = $url[2];
                unset($url[2]);
                unset($url[0]);
            }
        }

        /** Verifica se a url tem valor, caso não exita recebe um array vazio */
        $this->urlParam = $url ? array_values($url) : [];

        call_user_func_array([$this->urlController, $this->urlMethod], $this->urlParam);
    }

    public function parseURL(): array
    {
        return explode("/", filter_var($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL));
    }
}
