<?php

namespace App\Core;

/**
 * Controller padrão
 */
class Controller
{
    /**
     * <etodo reposnsavel por carregar as view e enviar parametros
     *
     * @param [type] $viewName
     * @param array $data
     * @return void
     */
    public function loadView($viewName, $data = [])
    {
        require_once '../App/View/' . $viewName . '.php';
        return $viewName;
    }
}
