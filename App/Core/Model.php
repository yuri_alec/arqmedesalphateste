<?php

namespace App\Core;

use PDO;

/**
 * Model padrão de conexão com o banco de dados
 */
class Model
{
    /**
     * Instancia da conexao
     *
     * @var [type]
     */
    private static $instance;

    public static function getConn(): object
    {
        if (!isset(self::$instance)) {
            self::$instance = new PDO('mysql:host=db;dbname=arqmedes_alpha_test;charset=UTF8', 'arqmedes_alpha', 12345678);
        }

        return self::$instance;
    }
}
