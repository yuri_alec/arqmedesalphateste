<?php

namespace App\Models;

use App\Core\Model;
use Exception;
use PDO;
use PDOException;

class Category extends Model
{
    /**Recebe os dados do banco de dados @var array */
    private array $resultBd;

    /**Recebe os dados da controller @var array */
    private array $data;

    /**Nome da categoria @var string */
    private string $name;

    /** Codigo da categoria @var integer*/
    private int $code;

    /** retorna true quando sucesso @var boolean */
    public bool $result;

    /** recebe o id por parametro @var integer */
    private int $id;

    public function getResult()
    {
        return $this->result;
    }

    public function getAll()
    {
        $query = "SELECT * FROM category";
        $stmt = Model::getConn()->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->resultBd = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->resultBd;
        } else {
            return [];
        }
    }

    public function addNew(array $data = [])
    {
        $this->data = $data;
        $this->name = $this->data['categoty_name'];
        $this->code = $this->data['category_code'];

        $query = "INSERT INTO category (name, code) VALUES (:name, :code)";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':code', $this->code);

        try {
            $stmt->execute();
        } catch (PDOException $err) {
            var_dump($err);
            die;
        }

        if ($stmt->rowCount() > 0) {
            $_SESSION['msg'] = "<p><span style='color: #008000'>
            Registered successfully!</span><p>";
            $this->result = true;
        } else {
            $_SESSION['msg'] = "<p><span style='color: #f00'>Couldn't save. Try again later.</span><p>";
            $this->result = false;
        }
    }

    public function getCategoryByid(int $id)
    {
        $this->id = $id;

        $query = "SELECT * FROM category WHERE id =:id LIMIT 1";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->resultBd = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->resultBd;
        } else {
            return [];
        }
    }

    public function update(array $data = [])
    {
        $this->data = $data;

        $this->id = $this->data['id'];
        $this->name = $this->data['categoty_name'];
        $this->code = $this->data['category_code'];

        $query = "UPDATE category SET name =:name, code =:code WHERE id =:id";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':code', $this->code);
        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()) {
            $_SESSION['msg'] = "<p><span style='color: #008000'>
            Category updated successfully!</span><p>";
            return true;
        } else {
            $_SESSION['msg'] = "<p><span style='color: #f00'>Couldn't update category. Try again later.</span><p>";
            return false;
        }
    }

    public function deleteCategory($id = '')
    {
        $this->id = (int) $id;
        $query = "DELETE FROM category WHERE id =:id";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()) {
            $_SESSION['msg'] = "<p><span style='color: #008000'>
            Category deleted successfully!</span><p>";
            return true;
        } else {
            $_SESSION['msg'] = "<p><span style='color: #f00'>Couldn't delete category. Try again later.</span><p>";
            return false;
        }
    }
}
