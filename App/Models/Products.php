<?php

namespace App\Models;

use App\Core\Model;
use PDO;
use PDOException;

class Products extends Model
{
    /**Recebe os dados do banco de dados @var array */
    private array $resultBd;

    /**Recebe os dados da controller @var array */
    private array $data;

    /** Dados */
    private $sku;
    private $name;
    private $price;
    private $quantity;
    private $categories;
    private $description;

    /** Recebe o arquivo para upload @var string */
    private string $file;

    /** Retorna true quando sucesso @var boolean */
    public bool $result;

    /** Recebe o total de registros @var */
    private $total;

    /** recebe o id por parametro @var integer */
    private int $id;

    public function getResult()
    {
        return $this->result;
    }


    public function getAll()
    {
        $query = "SELECT * FROM products";
        $stmt = Model::getConn()->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->resultBd = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->resultBd;
        } else {
            return [];
        }
    }

    public function add(array $data, string $file)
    {
        $this->data = $data;
        $this->file = $file;

        $this->sku = $this->data['sku'];
        $this->name = $this->data['name'];
        $this->price = (float) $this->data['price'];
        $this->quantity = (int) $this->data['quantity'];
        $this->categories = serialize($this->data['category']);
        $this->description = $this->data['description'];

        $query = "INSERT INTO products (name, sku, description, quantity, category_id, image, price)
                        VALUES (:name, :sku, :description, :quantity, :categories, :image, :price)";

        $stmt = Model::getConn()->prepare($query);

        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':quantity', $this->quantity);
        $stmt->bindParam(':categories', $this->categories);
        $stmt->bindParam(':image', $this->file);
        $stmt->bindParam(':price', $this->price);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->result = true;
        } else {
            $this->result = false;
        }
    }

    public function totalRocords()
    {
        $query = "SELECT COUNT(id) AS count FROM products";
        $stmt = Model::getConn()->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->total = $stmt->fetch(PDO::FETCH_ASSOC);
            return $this->total;
        } else {
            return '';
        }
    }

    public function delete($id = '')
    {
        $this->id = (int) $id;
        $query = "DELETE FROM products WHERE id =:id";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':id', $this->id);

        if ($stmt->execute()) {
            $this->result = true;
        } else {
            $this->result = false;
        }
    }

    public function getProductByid(int $id)
    {
        $this->id = $id;

        $query = "SELECT * FROM products WHERE id =:id LIMIT 1";
        $stmt = Model::getConn()->prepare($query);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->resultBd = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $this->resultBd;
        } else {
            return [];
        }
    }

    public function edit(array $data = [], string $file)
    {
        $this->data = $data;
        $this->file = $file;

        $this->id = $this->data['id'];
        $this->sku = $this->data['sku'];
        $this->name = $this->data['name'];
        $this->price = (float) $this->data['price'];
        $this->quantity = (int) $this->data['quantity'];
        $this->categories = serialize($this->data['category']);
        $this->description = $this->data['description'];

        try {
        } catch (\Throwable $th) {
            //throw $th;
        }
        $query = "UPDATE products
                    SET name =:name, sku =:sku, description =:description,
                        quantity =:quantity, category_id =:categories,
                        image =:image, price =:price
                    WHERE id =:id";

        $stmt = Model::getConn()->prepare($query);

        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':quantity', $this->quantity);
        $stmt->bindParam(':categories', $this->categories);
        $stmt->bindParam(':image', $this->file);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $this->result = true;
        } else {
            $this->result = false;
        }
    }
}
