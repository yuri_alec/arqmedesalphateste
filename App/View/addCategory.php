<!doctype html>
<html ⚡>

<head>
  <title>Arqmedes | Backend Test | Add Category</title>
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" media="all" href="/css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>
    body {
      -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-moz-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-ms-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-o-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }
  </style><noscript>
    <style amp-boilerplate>
      body {
        -webkit-animation: none;
        -moz-animation: none;
        -ms-animation: none;
        animation: none
      }
    </style>
  </noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.html"><img src="/images/arqmedes_logo-nova.jpg" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="/category/index" class="link-menu">Categorias</a></li>
      <li><a href="/home/index" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.html" class="link-logo"><img src="/images/php.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
  <h1 class="title new-item"><?php !empty($this->data['CategoryId']) ? printf("Edit Category") : printf("New Category") ?></h1>
  <?php
  if (!empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
  }
  ?>
  <form action="" method="POST">
    <?php if (isset($this->data['CategoryId'])) : ?>
      <input type="hidden" name="id" value="<?= $this->data['CategoryId'][0]['id'] ?>">
    <?php endif; ?>
    <div class="input-field">
      <label for="category-name" class="label">Category Name</label>
      <input type="text" id="category-name" name="categoty_name" class="input-text" value="<?php !empty($this->data['CategoryId']) ? printf($this->data['CategoryId'][0]['name']) : '' ?>" />
    </div>
    <div class="input-field">
      <label for="category-code" class="label">Category Code</label>
      <input type="text" id="category-code" name="category_code" class="input-text" value="<?php !empty($this->data['CategoryId']) ? printf($this->data['CategoryId'][0]['code']) : '' ?>" />

    </div>
    <?php if (!empty($this->data['CategoryId'])) : ?>
      <div class="actions-form">
        <a href="/category/edit" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" name="update" value="update" />
      </div>
    <?php else : ?>
      <div class="actions-form">
        <a href="/category/index" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" name="save" value="Save" />
      </div>
    <?php endif; ?>
  </form>
</main>
<!-- Main Content -->

<!-- Footer -->
<footer>
  <div class="footer-image">
    <img src="/images/arqmedes_logo-nova.jpg" width="119" height="26" alt="Go Jumpers" />
  </div>
  <div class="email-content">
    <span>ecio@arqmedesconsultoria.com.br</span>
  </div>
</footer>
<!-- Footer --></body>

</html>