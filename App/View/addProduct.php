<!doctype html>
<html ⚡>

<head>
  <title>Arqmedes | Backend Test | Add Product</title>
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" media="all" href="/css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>
    body {
      -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-moz-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-ms-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-o-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }
  </style><noscript>
    <style amp-boilerplate>
      body {
        -webkit-animation: none;
        -moz-animation: none;
        -ms-animation: none;
        animation: none
      }
    </style>
  </noscript>

  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <!-- JQuery -->
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="jquery.maskMoney.js" type="text/javascript"></script>

  <!-- Select2 -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <!-- MaskMoney -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" integrity="sha512-Rdk63VC+1UYzGSgd3u2iadi0joUrcwX0IWp2rTh6KXFoAmgOjRS99Vynz1lJPT8dLjvo6JZOqpAHJyfCEZ5KoA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script>
    $(document).ready(function() {
      $("#category_multiple").select2();

      $("#price").maskMoney({
        decimal: ".",
        thousands: "."
      });
    });
  </script>

</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/home/index"><img src="/images/arqmedes_logo-nova.jpg" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="/category/index" class="link-menu">Categorias</a></li>
      <li><a href="/home/index" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/home/index" class="link-logo"><img src="/images/php.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>
</header>
<!-- Header -->
<!-- Main Content -->
<main class="content">
  <?php
  if (!empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
  }

  $category = unserialize($this->data['ProductId'][0]['category_id']);
  ?>
  <h1 class="title new-item"><?php !empty($this->data['ProductId']) ? printf("Edit Product") : printf("New Product") ?></h1>
  <form action="" method="post" enctype="multipart/form-data">
    <?php if (isset($this->data['ProductId'])) : ?>
      <input type="hidden" name="id" value="<?= $this->data['ProductId'][0]['id'] ?>">
    <?php endif; ?>
    <div class="input-field">
      <label for="sku" class="label">Product SKU</label>
      <input type="text" id="sku" name="sku" class="input-text" value="<?php !empty($this->data['ProductId']) ? printf($this->data['ProductId'][0]['sku']) : '' ?>" required />
    </div>
    <div class="input-field">
      <label for="name" class="label">Product Name</label>
      <input type="text" id="name" name="name" class="input-text" value="<?php !empty($this->data['ProductId']) ? printf($this->data['ProductId'][0]['name']) : '' ?>" required />
    </div>
    <div class="input-field">
      <label for="price" class="label">Price</label>
      <input type="text" id="price" name="price" class="input-text" value="<?php !empty($this->data['ProductId']) ? printf($this->data['ProductId'][0]['price']) : '' ?>" required />
    </div>
    <div class="input-field">
      <label for="quantity" class="label">Quantity</label>
      <input type="text" id="quantity" name="quantity" class="input-text" value="<?php !empty($this->data['ProductId']) ? printf($this->data['ProductId'][0]['quantity']) : '' ?>" required />
    </div>

    <div class="input-field">

      <!-- <?php foreach ($category as $key => $value) : ?>
        <?php var_dump((int)$value); ?>
      <?php endforeach; ?> -->

      <label for="category" class="label">Categories</label>
      <select multiple name="category[]" id="category_multiple" class="input-text" required>


        <?php foreach ($this->data['categories'] as $categories) : ?>
          <?php extract($categories) ?>
          <option value="<?= (int) $id ?> "><?= $name ?></option>
        <?php endforeach; ?>

      </select>
    </div>

    <div class="input-field">
      <label for="description" class="label">Description</label>
      <textarea id="description" name="description" class="input-text" required>
      <?php !empty($this->data['ProductId']) ? printf($this->data['ProductId'][0]['description']) : '' ?>
      </textarea>
    </div>
    <div class="input-field">
      <label for="image" class="label">Image</label>
      <?php !empty($this->data['ProductId']) ? printf("<img src='/uploads/" . $this->data['ProductId'][0]['image'] . "'> ") : '' ?>
      <input type="file" id="image" name="image" class="input-text" />
    </div>
    <?php if (!empty($this->data['ProductId'])) : ?>
      <div class="actions-form">
        <a href="/products/edit" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" name="update" value="Update" />
      </div>
    <?php else : ?>
      <div class="actions-form">
        <a href="/products/index" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" name="addNewProduct" value="Save" />
      </div>
    <?php endif; ?>
  </form>
</main>
<!-- Main Content -->

<!-- Footer -->
<footer>
  <div class="footer-image">
    <img src="/images/arqmedes_logo-nova.jpg" width="119" height="26" alt="Go Jumpers" />
  </div>
  <div class="email-content">
    <span>ecio@arqmedesconsultoria.com.br</span>
  </div>
</footer>
<!-- Footer --></body>

</html>