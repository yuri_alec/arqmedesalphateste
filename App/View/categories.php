<!doctype html>
<html ⚡>

<head>
  <title>Arqmedes | Backend Test | Categories</title>
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" media="all" href="/css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>
    body {
      -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
      animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-moz-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-ms-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @-o-keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }

    @keyframes -amp-start {
      from {
        visibility: hidden
      }

      to {
        visibility: visible
      }
    }
  </style><noscript>
    <style amp-boilerplate>
      body {
        -webkit-animation: none;
        -moz-animation: none;
        -ms-animation: none;
        animation: none
      }
    </style>
  </noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/home/index"><img src="/images/arqmedes_logo-nova.jpg" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="/category/index" class="link-menu">Categorias</a></li>
      <li><a href="/home/index" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/home/index" class="link-logo"><img src="/images/php.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>
</header>
<!-- Header -->

<body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <?php
      if (!empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
      }
      ?>
      <a href="/category/add" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
          <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach ($this->data['Categories'] as $category) : ?>
        <?php extract($category) ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $name ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $code ?></span>
          </td>

          <td class="data-grid-td">
            <div class="actions">
              <div class="action edit">
                <span><a style="text-decoration: none;" href="/category/edit/<?= $id ?>">Edit</a></span>
              </div>
              <div class="action delete"><span><a style="text-decoration: none;" href="/category/delete/<?= $id ?>" onClick="return confirm('Are you sure you want to delete this record?')">Delete</a></span></div>
            </div>
          </td>
        </tr>
      <?php endforeach; ?>

    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
  <footer>
    <div class="footer-image">
      <img src="/images/arqmedes_logo-nova.jpg" width="119" height="26" alt="Go Jumpers" />
    </div>
    <div class="email-content">
      <span>ecio@arqmedesconsultoria.com.br</span>
    </div>
  </footer>
  <!-- Footer -->
</body>

</html>