<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'dbb82c7c724ec33a076fa51d22975355588e14ce',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'dbb82c7c724ec33a076fa51d22975355588e14ce',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'codeguy/upload' => array(
            'pretty_version' => '1.3.2',
            'version' => '1.3.2.0',
            'reference' => '6a9e5e1fb58d65346d0e557db2d46fb25efd3e37',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeguy/upload',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
